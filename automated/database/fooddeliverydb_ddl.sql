CREATE TABLE fd_restaurant
(
    rest_id int PRIMARY KEY NOT NULL AUTO_INCREMENT,
    name varchar(255) NOT NULL,
    description varchar(255),
    date_created timestamp NOT NULL,
    street varchar(255),
    city varchar(255),
    state varchar(255),
    country varchar(255),
    latitude int,
    longitude int
);
CREATE UNIQUE INDEX fd_restaurant_rest_id_uindex ON fd_restaurant (rest_id);

CREATE TABLE fd_food
(
    food_id int PRIMARY KEY NOT NULL AUTO_INCREMENT,
    name varchar(255) NOT NULL,
    description varchar(255),
    price double NOT NULL,
    id int,
    rest_id int NOT NULL,
    CONSTRAINT fd_food_fd_restaurant_rest_id_fk FOREIGN KEY (rest_id) REFERENCES fd_restaurant (rest_id)
);
CREATE UNIQUE INDEX fd_food_food_id_uindex ON fd_food (food_id);