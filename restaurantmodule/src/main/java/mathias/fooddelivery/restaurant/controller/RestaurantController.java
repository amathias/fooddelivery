package mathias.fooddelivery.restaurant.controller;

import mathias.fooddelivery.restaurant.model.pojos.Restaurant;
import mathias.fooddelivery.restaurant.services.RestaurantService;
import mathias.fooddelivery.util.JsonUtil;
import mathias.fooddelivery.util.ResponseMessage;
import org.eclipse.jetty.http.HttpStatus;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Controller;
import spark.Route;
import spark.Spark;

import javax.annotation.PostConstruct;

@Controller
@Order(1)
public class RestaurantController {

    @Autowired
    private RestaurantService restaurantService;

    @PostConstruct
    void init() {

        Spark.get(Path.RESTAURANT, getrestaurants);

        Spark.post(Path.API_RESTAURANT, addRestaurant);

        Spark.put(Path.API_RESTAURANT, updateRestaurant);

    }

    private Route getrestaurants = (request, response) -> {

        final Restaurant restaurant = JsonUtil.objectFrom(request.body(), Restaurant.class);

        if (restaurant != null) {

            restaurantService.list(restaurant.getLatitude(), restaurant.getLongitude());
        } else {
            restaurantService.list(null, null);
        }

        return JsonUtil.jsonFrom(new ResponseMessage(HttpStatus.OK_200, "Restaurants Listed!"));
    };
    private Route addRestaurant = (request, response) -> {

        final Restaurant restaurant = JsonUtil.objectFrom(request.body(), Restaurant.class);

        restaurantService.create(restaurant);

        return JsonUtil.jsonFrom(new ResponseMessage(HttpStatus.OK_200, "Restaurant Added!"));
    };
    private Route updateRestaurant = (request, response) -> {

        final Integer restId = Integer.valueOf(request.queryParams("restId"));
        final Restaurant restaurant = JsonUtil.objectFrom(request.body(), Restaurant.class);

        restaurantService.update(restId, restaurant);

        return JsonUtil.jsonFrom(new ResponseMessage(HttpStatus.OK_200, "Restaurant Updated!"));
    };

    private static class Path {

        static final String RESTAURANT = "/restaurant";
        static final String API_RESTAURANT = "/api/restaurant";
    }


}
