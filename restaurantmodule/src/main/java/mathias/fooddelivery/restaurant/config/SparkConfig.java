package mathias.fooddelivery.restaurant.config;

import mathias.fooddelivery.auth.TokenAuthenticationService;
import mathias.fooddelivery.util.JsonUtil;
import mathias.fooddelivery.util.ResponseMessage;
import org.eclipse.jetty.http.HttpStatus;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.annotation.Order;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import spark.Filter;
import spark.Spark;

import javax.annotation.PostConstruct;

@Configuration
@Order(1)
@PropertySource("classpath:application.properties")
public class SparkConfig {

    public static Filter checkAuthentication = (request, response) -> {

        request.session().removeAttribute(TokenAuthenticationService.HEADER_STRING);

        final String header = request.raw().getHeader(TokenAuthenticationService.HEADER_STRING);

        if (header == null || !header.startsWith(TokenAuthenticationService.TOKEN_PREFIX)) {

            Spark.halt(403);
            return;
        }

        final Authentication authentication = TokenAuthenticationService.getAuthentication(request.raw());

        request.session().attribute(TokenAuthenticationService.HEADER_STRING, authentication);

        SecurityContextHolder.getContext().setAuthentication(authentication);

    };
    private String serverPort;

    SparkConfig(@Value("${spark.server.port}") String serverPort) {
        this.serverPort = serverPort;
    }

    @PostConstruct
    void init() {

        Spark.port(Integer.parseInt(serverPort));

        Spark.before(Path.API, checkAuthentication);

        Spark.afterAfter((request, response) -> response.type("application/json"));

        Spark.exception(UsernameNotFoundException.class, (e, request, response) -> {
            e.printStackTrace();

            response.status(HttpStatus.UNAUTHORIZED_401);
            response.body(JsonUtil.jsonFrom(new ResponseMessage(HttpStatus.UNAUTHORIZED_401, e.getMessage())));

        });

        Spark.exception(RuntimeException.class, (e, request, response) -> {
            e.printStackTrace();
            response.status(HttpStatus.INTERNAL_SERVER_ERROR_500);
            response.status(HttpStatus.INTERNAL_SERVER_ERROR_500);
            response.body(JsonUtil.jsonFrom(new ResponseMessage(HttpStatus.INTERNAL_SERVER_ERROR_500, "Internal server error!")));
        });

//        Spark.get("*", (request, response) -> {
//            response.status(HttpStatus.NOT_FOUND_404);
//            return JsonUtil.jsonFrom( new ResponseMessage(HttpStatus.NOT_FOUND_404, "Not found!") );
//        });

    }

    private static class Path {
        private static final String API = "/api/*";
    }

}
