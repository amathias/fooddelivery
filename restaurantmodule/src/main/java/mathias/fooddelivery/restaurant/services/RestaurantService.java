package mathias.fooddelivery.restaurant.services;

import mathias.fooddelivery.restaurant.model.pojos.Restaurant;

import java.util.List;
import java.util.Optional;

public interface RestaurantService {

    List<Restaurant> list(Integer latitude, Integer longigute);

    Restaurant create(Restaurant restaurant);

    Optional<Restaurant> find(Integer restId);

    Optional<Restaurant> update(Integer restId, Restaurant restaurant);

}
