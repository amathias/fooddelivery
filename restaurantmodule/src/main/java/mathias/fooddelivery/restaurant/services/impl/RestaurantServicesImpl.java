package mathias.fooddelivery.restaurant.services.impl;

import mathias.fooddelivery.restaurant.model.daos.RestaurantDao;
import mathias.fooddelivery.restaurant.model.definitions.Tables;
import mathias.fooddelivery.restaurant.model.pojos.Restaurant;
import mathias.fooddelivery.restaurant.services.RestaurantService;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.rabbit.annotation.EnableRabbit;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
@EnableRabbit
public class RestaurantServicesImpl implements RestaurantService {

    @Autowired
    private RestaurantDao restaurantDao;

    @Override
    public List<Restaurant> list(Integer latitude, Integer longigute) {

        if (latitude == null || longigute == null) {
            return restaurantDao.findAll();
        }

        return null;
    }

    @Override
    public Restaurant create(Restaurant restaurant) {

        // TODO: Validation

        restaurantDao.insert(restaurant);
        // TODO: Verify if it has ID after being inserted.

        return restaurant;
    }

    @Override
    public Optional<Restaurant> find(Integer restId) {
        return restaurantDao.fetchOptional(Tables.FD_RESTAURANT.REST_ID, restId);
    }

    @Override
    public Optional<Restaurant> update(Integer restId, Restaurant restaurant) {

        restaurant.setRestId(restId);
        restaurantDao.update(restaurant);

        return Optional.empty();
    }

    @RabbitListener(queues = "${rabbitmq.queue.restaurantOrder}")
    public void insertUserListener(Message message) {

        // TODO: Process restaurant Order
        // Will receive complete order
        // Confirm order
    }


}
