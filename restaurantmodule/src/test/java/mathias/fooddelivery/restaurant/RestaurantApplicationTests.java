package mathias.fooddelivery.restaurant;

import com.mashape.unirest.http.HttpResponse;
import com.mashape.unirest.http.JsonNode;
import com.mashape.unirest.http.Unirest;
import org.eclipse.jetty.http.HttpStatus;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;
import spark.Spark;

public class RestaurantApplicationTests {

    private static final String BASE_ENDPOINT = "http://localhost:9090";

    private static final String RESTAURANT_ENDPOINT = "/restaurant";
    private static final String API_RESTAURANT_ENDPOINT = "/api/restaurant";

    @BeforeClass
    public static void start() {

        RestaurantApplication.main(new String[]{});

        Spark.awaitInitialization();
    }

    @Test
    public void testListRestaurants() throws Exception {

        final HttpResponse<JsonNode> jsonResponse = Unirest.get(BASE_ENDPOINT + RESTAURANT_ENDPOINT)
                .header("accept", "application/json").asJson();

        Assert.assertEquals("Status code is not OK!", HttpStatus.OK_200, jsonResponse.getStatus());

        final JsonNode json = jsonResponse.getBody();

        Assert.assertEquals("", json.getObject().getString("message"), "Restaurants Listed!");
    }

    @Test
    public void testAddRestaurants() throws Exception {

        final String jsonBody = "{ restaurant: \"Subway\" }";

        final HttpResponse<JsonNode> jsonResponse = Unirest.post(BASE_ENDPOINT + API_RESTAURANT_ENDPOINT)
                .header("accept", "application/json")
                .body(jsonBody)
                .asJson();

        Assert.assertEquals("Status code is not Forbidden!", HttpStatus.FORBIDDEN_403, jsonResponse.getStatus());
    }

    @Test
    public void testUpdateRestaurants() throws Exception {

        final String jsonBody = "{ restaurant: \"Subway\" }";


        final HttpResponse<JsonNode> jsonResponse = Unirest.post(BASE_ENDPOINT + API_RESTAURANT_ENDPOINT)
                .header("accept", "application/json")
                .body(jsonBody)
                .asJson();

        Assert.assertEquals("Status code is not Forbidden!", HttpStatus.FORBIDDEN_403, jsonResponse.getStatus());
    }

}
