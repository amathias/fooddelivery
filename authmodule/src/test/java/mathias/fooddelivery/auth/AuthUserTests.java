package mathias.fooddelivery.auth;

import mathias.fooddelivery.auth.model.AuthUser;
import mathias.fooddelivery.auth.repository.UserRepository;
import mathias.fooddelivery.auth.service.UserService;
import org.hamcrest.CoreMatchers;
import org.junit.After;
import org.junit.Assert;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Optional;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = AuthApplication.class)
public class AuthUserTests {

    @Rule
    public ExpectedException exceptions = ExpectedException.none();
    @Autowired
    UserService userService;
    @Autowired
    UserRepository userRepository;

    @Test
    public void testInsertUser() {

        // SETUP
        Optional<AuthUser> usertest1 = userRepository.findByUsername("usertest1");
        usertest1.ifPresent(userRepository::delete);

        final AuthUser user = new AuthUser("usertest1", "usertest1");

        userService.insert(user);

        Assert.assertNotNull("User has ID null!", user.getUid());
        System.out.printf("User's UUID: %s", user.getUid());

        // TEARDOWN
        usertest1 = userRepository.findByUsername("usertest1");
        usertest1.ifPresent(userRepository::delete);

    }

    @Test
    public void testFetchUser() {

        // SETUP
        final AuthUser _usertest2 = new AuthUser("usertest2", "usertest2");
        userRepository.save(_usertest2);

        final String username = "usertest2";

        final Optional<AuthUser> user = userRepository.findByUsername(username);

        Assert.assertTrue("User is null!", user.isPresent());
        Assert.assertEquals("User fetched is different!", username, user.get().getUsername());

        System.out.printf("User: %s", user.get());

        // TEARDOWN
        final Optional<AuthUser> usertest2 = userRepository.findByUsername("usertest2");
        usertest2.ifPresent(userRepository::delete);

    }

    @Test
    public void testDeleteUser() {

        // SETUP
        final AuthUser usertest3 = new AuthUser("usertest3", "usertest3");
        userRepository.save(usertest3);

        Optional<AuthUser> user = userRepository.findByUsername("usertest3");
        Assert.assertTrue("User should be present!", user.isPresent());

        userRepository.delete(user.get());

        user = userRepository.findByUsername("usertest3");

        Assert.assertFalse("User still exists!", user.isPresent());

        // TEARDOWN
        final Optional<AuthUser> _usertest3 = userRepository.findByUsername("usertest3");
        _usertest3.ifPresent(userRepository::delete);
    }

    @Test
    public void testInsertEmptyUsername() {

        exceptions.expect(IllegalArgumentException.class);
        exceptions.expectMessage(CoreMatchers.equalTo("User's username cannot be empty!"));

        final AuthUser usertest4 = new AuthUser("", "usertest");
        userService.insert(usertest4);
    }

    @Test
    public void testInsertEmptyPassword() {

        exceptions.expect(IllegalArgumentException.class);
        exceptions.expectMessage(CoreMatchers.equalTo("User's password cannot be empty!"));

        final AuthUser usertest4 = new AuthUser("usertest", "");
        userService.insert(usertest4);
    }

    @Test
    public void testInsertDuplicatedUser() {

        // SETUP
        final AuthUser usertest4 = new AuthUser("usertest4", "usertest4");
        userRepository.save(usertest4);

        exceptions.expect(IllegalStateException.class);
        exceptions.expectMessage(CoreMatchers.containsString("username"));

        final AuthUser user = new AuthUser("usertest4", "usertest4");
        userService.insert(user);
    }

    @After
    public void testInsertDuplicatedUserTearDown() {
        final Optional<AuthUser> usertest4 = userRepository.findByUsername("usertest4");
        usertest4.ifPresent(userRepository::delete);
    }

}
