package mathias.fooddelivery.auth;


import mathias.fooddelivery.auth.model.AuthUser;
import mathias.fooddelivery.auth.repository.UserRepository;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.amqp.core.Queue;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Optional;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = AuthApplication.class)
@AutoConfigureMockMvc
public class MessagesTest {

    @Autowired
    private RabbitTemplate rabbitTemplate;

    @Autowired
    @Qualifier("newUserQueue")
    private Queue orderQueue;

    @Autowired
    private UserRepository userRepository;

    @Test
    public void testSendMessage() throws Exception {

        // SETUP
        Optional<AuthUser> usertest1 = userRepository.findByUsername("usermessage1");
        usertest1.ifPresent(userRepository::delete);

        final AuthUser user = new AuthUser("usermessage1", "usermessage1");

        this.rabbitTemplate.convertAndSend(this.orderQueue.getName(), user);

        // TEARDOWN
        Thread.sleep(1000);
        final Optional<AuthUser> _testauth1 = userRepository.findByUsername("usermessage1");
        _testauth1.ifPresent(userRepository::delete);
    }

}
