package mathias.fooddelivery.auth;


import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.exc.MismatchedInputException;
import mathias.fooddelivery.auth.model.AuthUser;
import mathias.fooddelivery.auth.repository.UserRepository;
import org.hamcrest.CoreMatchers;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import java.util.Optional;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = AuthApplication.class)
@AutoConfigureMockMvc
public class AuthApplicationTests {

    private static final String AUTH_ENDPOINT = "/auth";

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private UserRepository userRepository;

    @Test(expected = MismatchedInputException.class)
    public void testAuthNoBody() throws Exception {

        this.mockMvc.perform(MockMvcRequestBuilders.post(AUTH_ENDPOINT))
                .andExpect(MockMvcResultMatchers.status().isForbidden());
    }

    @Test
    public void testAuth() throws Exception {

        // SETUP
        final AuthUser testauth1 = new AuthUser("testauth1", "$2a$04$GCxzgPgg88bB6Y6h7gWjUe4qAhLKUsXR3Q4DXPew4A6g9C3xtnbLm");
        userRepository.save(testauth1);

        final AuthUser user = new AuthUser("testauth1", "password");
        final String bodyContent = new ObjectMapper().writeValueAsString( user );

        this.mockMvc.perform(MockMvcRequestBuilders.post(AUTH_ENDPOINT).content(bodyContent))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.header().exists(TokenAuthenticationService.HEADER_STRING))
                .andExpect(MockMvcResultMatchers.header().string(TokenAuthenticationService.HEADER_STRING, CoreMatchers.containsString("Bearer")));

        // TEARDOWN
        final Optional<AuthUser> _testauth1 = userRepository.findByUsername("testauth1");
        _testauth1.ifPresent(userRepository::delete);
    }

    @Test
    public void testAuthWrongPass() throws Exception {

        // SETUP
        final AuthUser testauth2 = new AuthUser("testauth2", "$2a$04$GCxzgPgg88bB6Y6h7gWjUe4qAhLKUsXR3Q4DXPew4A6g9C3xtnbLm");
        userRepository.save(testauth2);

        final AuthUser user = new AuthUser("admin", "wrongpass");
        final String bodyContent = new ObjectMapper().writeValueAsString( user );

        this.mockMvc.perform(MockMvcRequestBuilders.post(AUTH_ENDPOINT).content( bodyContent ))
                .andExpect(MockMvcResultMatchers.status().isUnauthorized())
                .andExpect(MockMvcResultMatchers.header().doesNotExist("Authorization"));

        // TEARDOWN
        final Optional<AuthUser> _testauth2 = userRepository.findByUsername("testauth2");
        _testauth2.ifPresent(userRepository::delete);
    }

    @Test
    public void testAuthNonExistentUser() throws Exception {

        final AuthUser user = new AuthUser("nonexistent", "password");
        final String bodyContent = new ObjectMapper().writeValueAsString( user );

        this.mockMvc.perform(MockMvcRequestBuilders.post(AUTH_ENDPOINT).content( bodyContent ))
                .andExpect(MockMvcResultMatchers.status().isUnauthorized())
                .andExpect(MockMvcResultMatchers.header().doesNotExist("Authorization"));

    }

}
