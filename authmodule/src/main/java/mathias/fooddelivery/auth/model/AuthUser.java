package mathias.fooddelivery.auth.model;


import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Objects;
import java.util.UUID;

@Entity
@Table(name = "fd_auth_user")
public class AuthUser implements Serializable {

    @Id
    @GeneratedValue(generator = "uuid2")
    @GenericGenerator(name = "uuid2", strategy = "uuid2")
    @Column(name = "auus_uuid", columnDefinition = "BINARY(16)")
    private UUID uid;

    @Column(name = "auus_username", nullable = false)
    private String username;

    @Column(name = "auus_password", nullable = false)
    private String password;

    public AuthUser() {
    }

    public AuthUser(String username, String password) {
        this.username = username;
        this.password = password;
    }

    public UUID getUid() {
        return uid;
    }

    public void setUid(UUID uid) {
        this.uid = uid;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        AuthUser authUser = (AuthUser) o;
        return Objects.equals(uid, authUser.uid) &&
                Objects.equals(username, authUser.username) &&
                Objects.equals(password, authUser.password);
    }

    @Override
    public int hashCode() {

        return Objects.hash(uid, username, password);
    }

    @Override
    public String toString() {
        return "AuthUser{" +
                "uid=" + uid +
                ", username='" + username + '\'' +
                '}';
    }
}
