package mathias.fooddelivery.auth.service;

import mathias.fooddelivery.auth.model.AuthUser;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.stereotype.Service;

@Service
public interface UserService extends UserDetailsService {

    AuthUser insert(AuthUser user);
}
