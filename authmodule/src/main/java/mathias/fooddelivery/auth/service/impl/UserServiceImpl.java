package mathias.fooddelivery.auth.service.impl;

import com.fasterxml.jackson.databind.ObjectMapper;
import mathias.fooddelivery.auth.model.AuthUser;
import mathias.fooddelivery.auth.repository.UserRepository;
import mathias.fooddelivery.auth.service.UserService;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.rabbit.annotation.EnableRabbit;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCrypt;
import org.springframework.stereotype.Service;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.util.Optional;

@Service
@Qualifier("UserService")
@EnableRabbit
public class UserServiceImpl implements UserService {

    @Autowired
    private UserRepository userRepository;

    @Override
    public UserDetails loadUserByUsername(final String username) throws UsernameNotFoundException {

        final Optional<AuthUser> user = this.userRepository.findByUsername(username);

        if ( user.isPresent() ) {

            return org.springframework.security.core.userdetails.User.withUsername( user.get().getUsername() )
                    .password( user.get().getPassword() )
                    .roles("USER")
                    .build();

        }

        throw new UsernameNotFoundException("User not found!");
    }

    /**
     * Inserts a new user. Before that, hashes the password to be inserted in the database.
     *
     * @param user
     * @return The user inserted.
     */
    @Override
    public AuthUser insert(AuthUser user) {

        if (user == null) {
            throw new NullPointerException("User cannot be null!");
        }

        if (user.getUsername() == null || user.getUsername().trim().isEmpty()) {
            throw new IllegalArgumentException("User's username cannot be empty!");
        }

        if (user.getPassword() == null || user.getPassword().trim().isEmpty()) {
            throw new IllegalArgumentException("User's password cannot be empty!");
        }

        final Optional<AuthUser> _dbuser = userRepository.findByUsername(user.getUsername());
        if (_dbuser.isPresent()) {
            throw new IllegalStateException("There is already a User with the same username");
        }

        user.setPassword(BCrypt.hashpw(user.getPassword(), BCrypt.gensalt()));

        return userRepository.save(user);
    }

    @RabbitListener(queues = "${rabbitmq.queue.newUser}")
    public void insertUserListener(Message message) throws IOException {

        final AuthUser user = new ObjectMapper().readValue(new ByteArrayInputStream(message.getBody()), AuthUser.class);

        this.insert(user);
    }

}
