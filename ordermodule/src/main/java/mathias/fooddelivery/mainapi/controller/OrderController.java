package mathias.fooddelivery.mainapi.controller;

import mathias.fooddelivery.mainapi.util.ResponseMessage;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.security.Principal;

@RestController
public class OrderController {

    @PostMapping("/order")
    ResponseEntity placeOrder(Principal principal, @RequestBody Object order ) {

        return ResponseEntity.ok(new ResponseMessage(HttpStatus.OK.value(), "Order placed!"));

    }

}
