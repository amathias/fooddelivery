package mathias.fooddelivery.util;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

public class JsonUtil {

    public static final String OBJECT_BEGIN = "{";
    public static final String OBJECT_END = "}";
    public static final String ARRAY_BEGIN = "[";
    public static final String ARRAY_END = "]";

    public static Gson getGsonBulder() {

        final Gson gson = new GsonBuilder()
//                .setDateFormat( "yyyy-MM-dd" )
                .create();

        return gson;
    }

    public static <T> List<T> listFrom(final String jsonArray, final Class<T> clazz) {

        List<T> lista = new ArrayList<>();

        if (jsonArray.startsWith(ARRAY_BEGIN) && jsonArray.endsWith(ARRAY_END)) {

            lista = getGsonBulder().fromJson(jsonArray, new ParameterType<T>(clazz));
        }

        return lista;
    }

    public static <T> T objectFrom(final String jsonObject, final Class<T> clazz) {

        T object = null;

        if (jsonObject.startsWith(OBJECT_BEGIN) && jsonObject.endsWith(OBJECT_END)) {

            object = getGsonBulder().fromJson(jsonObject, clazz);
        }

        return object;
    }

    public static String jsonFrom(Object object) {
        return getGsonBulder().toJson(object);
    }

    private static class ParameterType<P> implements ParameterizedType {

        private Class<?> wrapped;

        public ParameterType(Class<P> wrapped) {
            this.wrapped = wrapped;
        }

        public Type[] getActualTypeArguments() {
            return new Type[]{wrapped};
        }

        public Type getRawType() {
            return List.class;
        }

        public Type getOwnerType() {
            return null;
        }

    }

}
